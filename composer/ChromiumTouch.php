<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//
require_once 'Actions/Plugin/Skeleton.php';

class Actions_Plugin_ChromiumTouch extends Actions_Plugin_Skeleton
{
    public function Actions_Plugin_ChromiumTouch ()
    {
        $this->_name        = 'Chromium Touch';
    }

    public function getName ()
    {
        return ($this->getTranslation ('Chromium Touch'));
    }

    public function getContextProperties ($context)
    {
        return (NULL);
    }

    public function getPlayerCommand ($hash)
    {
        return (false);
    }
}
